class CreateScores < ActiveRecord::Migration[5.2]
  def change
    create_table :scores do |t|
      t.string :reader
      t.integer :body
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
