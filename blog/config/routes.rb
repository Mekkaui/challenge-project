Rails.application.routes.draw do
  get 'welcome/index'
 
  resources :articles do
  	resources :comments
  end

  resources :articles do
    resources :scores
  end
 
  root 'welcome#index'
end
