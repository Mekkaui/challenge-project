class ScoresController < ApplicationController
  def create
    @score = Score.new(article_params)
 
    @score.save
    redirect_to @score
  end

  def new
    @score = Score.new
  end

  private
    def score_params
      params.require(:score).permit(:reader, :body)
    end
end
